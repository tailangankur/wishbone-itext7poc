package com.itext7POCExamples;
import java.io.FileOutputStream;
import java.io.StringWriter;
import java.util.Map;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;


public class htmlstringwithimagetopdf {
	
	  private VelocityEngine velocityEngine;

	  public htmlstringwithimagetopdf(VelocityEngine velocityEngine) {
		this.velocityEngine = velocityEngine;
	}
	  
	  
	  private String processVelocityTemplate(Map<String, Object> parameters, String templatePath) {

	        velocityEngine.init();
	        Template t = velocityEngine.getTemplate(templatePath);

	        VelocityContext context = new VelocityContext();
	        for (String key : parameters.keySet()) {
	            context.put(key, parameters.get(key));
	        }

	        StringWriter writer = new StringWriter();
	        t.merge(context, writer);

	        return writer.toString();
	    }
	 
	 
	  public void createPdf(Map<String, Object> parameters, String templatepath, String dest,String baseUri) {
	        try {
	        	ConverterProperties properties = new ConverterProperties();
	            properties.setBaseUri(baseUri);
	            String htmlStr = processVelocityTemplate(parameters, templatepath);
	            HtmlConverter.convertToPdf(htmlStr, new FileOutputStream(dest), properties);
//	            HtmlConverter.convertToPdf(htmlStr, new FileOutputStream(dest));

	        } catch (Exception e) {
	            e.printStackTrace();
	        }

	    }
	  
	  

//	  	public void createPdf(String baseUri, String src, String dest) throws IOException {
//	        HtmlConverter.convertToPdf(new File(src), new File(dest));
//	    }
	  
	

}
