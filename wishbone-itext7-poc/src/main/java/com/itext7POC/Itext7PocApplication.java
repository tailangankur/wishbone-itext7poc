package com.itext7POC;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;

import com.itext7POCExamples.htmlstringtopdf;
import com.itext7POCExamples.htmlstringwithimagetopdf;

@SpringBootApplication
public class Itext7PocApplication {
	
	/** The Base URI of the HTML page. */
	public static final String BASEURI = "src/main/resources/templates/";
	/** The path to the source HTML file. */
	public static final String SRC = String.format("%sview_receipt.html", BASEURI);
	/** The target folder for the result. */
	public static final String TARGET = "target/results/";
	/** The path to the resulting PDF file. */
	public static final String DEST = String.format("%s%s%s.pdf", TARGET, "view_receipt", new Date().getTime());
	
	public static final String TEMPLATE_PATH=new ClassPathResource("templates/view_receipt.vm").getPath();
	
	//public static String HTML = "<h1>Testing iText7</h1><p>Hello and Welcome to iText7</p>";
	
	@Autowired
	private VelocityEngine velocityEngine;
	
	public static void main(String[] args) {
		SpringApplication.run(Itext7PocApplication.class, args);
	}

	@PostConstruct
	public void run() {
		
		File file = new File(TARGET);
		file.mkdirs();
		//new htmlstringtopdf().createPdf(HTML, DEST);
		
		HashMap<String, Object> variables = new HashMap<>();
		variables.put("product", "image/amazon_prime.jpg");
        variables.put("leaf", "image/leaf.jpg");
        variables.put("order_id", "3545-6321-3732-6064");
        variables.put("order_title", "Deal title");
        variables.put("optionname", "7 Kickboxing Classes");
        variables.put("discountprice", "$" + "150.0");
        variables.put("originalprice", "$" + "75.0");
        variables.put("quantity", "2");
        variables.put("totalamount", "$" + "80.0");
        variables.put("billingdetails_line1", "Wishbone Club");
        variables.put("billingdetails_line2", "4002 South Avenue");
        variables.put("billingdetails_city", "Chicago");
        variables.put("billingdetails_state", "Illinois");
        variables.put("billingdetails_country", "US");
        variables.put("billingdetails_postal_code", "606161");
        variables.put("billingdetails_phoneno", "+919986995776");
		try {
			new htmlstringwithimagetopdf(velocityEngine).createPdf(variables, TEMPLATE_PATH, DEST,BASEURI);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	 	@Bean
	    VelocityEngine velocityEngine() throws IOException {
	        Properties properties = new Properties();
	        // properties.load(this.getClass().getResourceAsStream("/application.yml"));
	        properties.setProperty("resource.loader", "class");
	        properties.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
	        return new VelocityEngine(properties);
	    }

}
